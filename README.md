# Amazon Connect integration with Xdroid Voiceanalytics platform

This repository contains resources to integrate your Amazon Connect call center with Xdroid Voice Analytics platform.
The Cloudformation template provided in this guide configures a set of AWS resources that you need. Please follow Amazon Connect Xdroid integration - Quick Start Guide, Option 1: Automated process using CloudFormation templates that will guide you through how to use resources in this repository.


VoiceAnalytics is a voice and data analysis platform based on advanced artificial intelligence technology. The system processes and analyzes digitally stored audio and it’s metadata provided by a customer’s recording system. It provides reports, overview dashboards, and insights based on analysis results such as productivity metrics (speech vs. music vs. silence vs. overlapping speech), speech style indications (displeased vs. happy), speech characteristics (intonation, articulation, speech rate, speech volume, and pitch) and keyword, expression, and phrase detections. These metrics, potentially combined with other metadata, can also be used for predictive analytics, which is also part of VoiceAnalytics.

![Architecture Xdroid Amazon Connect Integration](https://gitlab.com/xdroid-public/xdroid-amazon-connect-integration/-/raw/main/img/amazonconnect_xdroid_overview.png)

This guide provides an example only, as your company may already have a configured Amazon Connect and/or using recording or additional AWS services. Feel free to contact us at support@xdroid.com for help.
